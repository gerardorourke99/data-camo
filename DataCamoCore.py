#Data Camo Core, updated by Jerry O'Rourke
#\Version 1.2
#Took all user interface out of DataCamoCore
#and moved the command line interface to
#DataCamoCMD. Rewrote core so that it is more of a
#back end function that both a command line interface
#and the prospective GUI can interface with.

#import stenganography library
from stegano import lsb


# this is our core component class
# we will add other components on dynamically via the decorator pattern

class DataCamoCore:

    #Class constructor
    def __init__(self):
        # input_file and output_file for using in creating and storing
        self.input_file = None
        self.output_file = None
        self.end_location = None
        
        # file_hold for general holding purposes, such as reveal in string. Kind of like of temp.
        self.file_hold = None

    #End constructor


    #string function
    def string_in_photo(self, input_file, string, end_location):

       #Assign function arguments to instance variables
       self.input_file = input_file
       self.end_location = end_location

       #encode string in file
       self.output_file = lsb.hide(self.input_file, string)
       self.output_file.save(self.end_location)

    #end string function



    

    #reveal string function
    def reveal_string(self, file_hold):
        self.file_hold = file_hold
        print("\n\n")
        print(lsb.reveal(self.file_hold))
        print("\n\n")
    #end reveal string function

#end DataCamoCore class
       
