import DataCamoCore

#A class to hold the command line interface.
class DataCamoCMD():

    
    @classmethod
    def main_menu(self):
        while True:
            print("---------Welcome to Data Camo---------\n")
            print("Please select an option")
            print("-----------------------\n")
            print("1 - Hide a string in a photo")
            print("2 - Reveal a hidden string")
            print("H - Help")
            print("Q - Quit\n")
            option = input("Selection: ")

            if option == "1":
                DataCamoCMD().string_in_photo()

            elif option == "2":
                DataCamoCMD().reveal_string()

            elif option == "H":
                DataCamoCMD().help_function()

            elif option == "Q":
                return 0
    
            else:
                print("Error: invalid input. Please try again\n\n")

    @classmethod
    def string_in_photo(self):
        print("\n\n\nPlease enter the file path for the photo you want to use\n")
        print("Note: Please use two backslashes C:\\Users\\user\\Desktop")
        print("Also, when entering")
        print("the file path, bear in mind that Python sets the current")
        print("working directory in the same directory that the script is")
        print("being run it.\n\n")

        input_file = input("File Path: ")
        print()
        string = input("Please enter the string you want to hide: ")
        print()
        print("Please input the name and location of where you want")
        print("the secret file saved (i.e. './secretFile.png')")
        print()
        end_location = input("File path: ")

        temp = DataCamoCore.DataCamoCore()
        temp.string_in_photo(input_file, string, end_location)

    def reveal_string(self):
        
        print("Please enter the file path of the file you want to reveal.\n")
        filePath = input("File Path: ")

        temp = DataCamoCore.DataCamoCore()
        temp.reveal_string(filePath)
        
#end DataCamoCMD

DataCamoCMD.main_menu()
